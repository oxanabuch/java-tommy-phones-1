package ua.kv.bucher.oxana;

public class Main {
    public static void main(String[] args) {

        NokiaPhone nokiaPhone = new NokiaPhone("Nokia", "black", "3310", 16, 1200, "Nokia Series");
        nokiaPhone.setPhoneNumber("12323253406");
        nokiaPhone.setText("Call me later");
        nokiaPhone.call();
        nokiaPhone.sendMessage();
        System.out.println("----------");


        SamsungPhone samsungPhone = new SamsungPhone("Samsung", "gold", "Galaxy S10", 128, 2000, "Android");
        samsungPhone.setPhoneNumber("4235723650239");
        samsungPhone.setText("I`m busy");
        samsungPhone.call();
        samsungPhone.sendMessage();
        samsungPhone.takePhoto();
        samsungPhone.recordVideo();
        System.out.println("----------");

        IPhone iPhone = new IPhone("IPhone", "blue", "13 Pro Max", 512, 3000, "IOS");
        iPhone.setPhoneNumber("1245938257033");
        iPhone.setText("Hello!");
        iPhone.call();
        iPhone.sendMessage();
        iPhone.takePhoto();
        iPhone.recordVideo();
        iPhone.sendMessageAll();
        System.out.println("----------");

    }
}
