package ua.kv.bucher.oxana;

public abstract class Phone {

    private String brand;
    private String color;
    private String modelName;
    private int memory;
    private int batteryCapacity;
    private String operationSystem;

    public Phone(String brand, String color, String modelName, int memory, int batteryCapacity, String operationSystem) {
        this.brand = brand;
        this.color = color;
        this.modelName = modelName;
        this.memory = memory;
        this.batteryCapacity = batteryCapacity;
        this.operationSystem = operationSystem;
    }
}
