package ua.kv.bucher.oxana;

public class IPhone extends Phone implements PhoneMedia, PhoneConnection {

    private String phoneNumber;
    private String text;

    public IPhone(String brand, String color, String modelName, int memory, int batteryCapacity, String operationSystem) {
        super(brand, color, modelName, memory, batteryCapacity, operationSystem);
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void call() {
        System.out.println("I'm calling using IPhone to the number: " + phoneNumber);
    }

    @Override
    public void sendMessage() {
        System.out.println("I'm sending message to from IPhone: " + text);

    }

    @Override
    public void takePhoto() {
        System.out.println("I'm taking a photo by IPhone");
    }

    @Override
    public void recordVideo() {
        System.out.println("I'm recording video by IPhone");
    }

    public void sendMessageAll() {
        System.out.println("I'm sending message by IPhone to all messengers in social nets");
    }
}
