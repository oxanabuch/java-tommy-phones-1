package ua.kv.bucher.oxana;

public interface PhoneMedia {

    void takePhoto();
    void recordVideo();
}
