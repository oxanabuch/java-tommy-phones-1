package ua.kv.bucher.oxana;

public interface PhoneConnection {

    void call();
    void sendMessage();
}
