package ua.kv.bucher.oxana;

public class NokiaPhone extends Phone implements PhoneConnection {

    private String phoneNumber;
    private String text;

    public NokiaPhone(String brand, String color, String modelName, int memory, int batteryCapacity, String operationSystem) {
        super(brand, color, modelName, memory, batteryCapacity, operationSystem);
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void call() {
        System.out.println("I'm calling using Nokia to the number: " + phoneNumber);
    }

    @Override
    public void sendMessage() {
        System.out.println("I'm sending message to from Nokia: " + text);
    }
}
